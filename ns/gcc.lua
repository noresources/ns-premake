--[[
ns-premake - gcc.lua
Informations about installed gcc and gcc-compliant programs

Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local core = require("ns.core")
local version = core.version 

local module = {}

local defaultBinaryPath = ""
local installedVersions = {}

local function retrieveVersion(binaryPath)
	local cmd = "echo \"\" | " ..  binaryPath .. " -E -dM -x c - | grep __GNU"
	local output = os.outputof(cmd)
	if (type(output) == "string") and (#output > 0)
	then
		local output = string.explode(output, "\n")
		local result = {major = 0, minor = 0, patch = 0}
	
		local rules = 
		{
			{
				keyword = "__GNUC_PATCHLEVEL__",
				key = "patch"
			},
			{
				keyword = "__GNUC_MINOR__",
				key = "minor"
			},
			{
				keyword = "__GNUC__",
				key = "major"
			},
		}
		
		for i,v in pairs(output)
		do
			for j, r in pairs (rules)
			do
				if string.find(v, r.keyword) ~= nil
				then
					local pattern = ".*" .. r.keyword .. "%s-([%d]+)"
					local res = string.gsub(v, pattern, "%1")
					rawset(result, r.key, tonumber(res))
				end
			end
		end
		
		for j, r in pairs (rules)
		do
			if rawget(result, r.key) == nil
			then
				rawset(result, r.key, 0)
			end
		end
		
		result.versionstring = version.tostring(result)
		result.versionnumber = version.tonumber(result)
		result.path = path.getdirectory(binaryPath)
		result.name = path.getname(binaryPath)
		result.fullpath = binaryPath 
				
		return result
	end
	
	return nil
end

local function initialize()
	local output = os.outputof("which gcc | xargs echo -n")
	local d = path.getdirectory(output)
	if (type(output) == "string") and os.isdir(d)
	then
		defaultBinaryPath = output
		installedVersions[defaultBinaryPath] = retrieveVersion(defaultBinaryPath)
	end
end

local function getInstalledVersion(path)
	local p = path
	if (type (p) == "string")
	then
		p = path.getabsolute(p)
	else
		p = defaultBinaryPath
	end
	
	if os.isdir(p)
	then
		p = p .. "/gcc"
	end
	
	local i = installedVersions[p]
	if i == nil
	then
		installedVersions[p] = retrieveVersion(p)
		i = installedVersions[p]
	end
	
	return i
end

initialize()

-- Public API

function module.getinfo(path)
	-- Get gcc info as an associative array  
	return getInstalledVersion(path)
end

function module.getpath()
	-- Get the default gcc path
	return defaultBinaryPath
end

return module
