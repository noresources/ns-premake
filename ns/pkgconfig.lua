--[[
ns-premake - pkgconfig.lua
pkg-config utility wrapper

Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local core = require("ns.core")
local version = core.version
local cmd = require("ns.cmd")
local pkgconfigcmd = cmd["pkg-config"]

local function parseoptionlines(line, prefix)
	-- TOOD support paths with spaces (-I"/path with/spaces")
	
	local result = {}
	local pattern = "%s*" .. prefix .. "(%S+)"
	for opt in string.gmatch(line, pattern)
	do
		local s, n = string.gsub(opt, pattern, "%1")
		table.insert(result, s)
	end
	
	return result
end

local function setupproject(pkg)
	if not (pkg.exists)
	then
		return
	end
	
	for _, key in ipairs({
			'sysincludedirs', 
			'includedirs', 
			'buildoptions', 
			'syslibdirs', 
			'libdirs', 
			'linkoptions', 
			'links',
			'runpathdirs'
		})
	do
		local v = pkg[key]
		if (type(v) == "string" or type(v) == "table")
			and (#v > 0)
		then
			_G[key](v)
		end
	end
end

local this = 
{
}

-- Public API

function this.exists(moduleName)
	-- Indicates if the given module exists
	if not (pkgconfigcmd.exists)
	then
		return false 
	end
	
	return (tonumber(pkgconfigcmd.execute("--exists " .. tostring(moduleName))) == 0)
end

function this.getelement(moduleName, element)
	-- Query a pkg-config field
	if not (pkgconfigcmd.exists)
	then
		return ''
	end
	
	return pkgconfigcmd.outputfirstlineof("--" .. tostring(element) .. " " .. moduleName .. " 2>/dev/null")
end

function this.get(moduleName)
	-- Get package information.
	-- Return a table containing package information,
	-- 	premake instructions and a setupproject() method
	-- 	to easily add the required premake instructions 
	local result = {
		name = moduleName,
		exists = this.exists(moduleName),
		versionstring = '',
		versionnumber = 0,
		major = 0,
		minor = 0,
		patch = 0,
		sysincludedirs = {},
		includedirs = {},
		buildoptions = {},
		syslibdirs = {},
		libdirs = {},
		runpathdirs = {},
		links = {},
		linkoptions = {}
	}
		
	if not(result.exists)
	then
		return result
	end
	
	result.versionstring = this.getelement(moduleName, "modversion")
	local parts = { "major", "minor", "patch" }
	local p = version.toparts(result.versionstring)
	for k, v in pairs(p) do result[k] = v end
	
	-- Get the full version string
	result.versionstring = version.tostring(p, true) 
	
	result.buildoptions = this.getelement(moduleName, "cflags-only-other")
	result.buildoptions = { result.buildoptions }
	result.linkoptions = this.getelement(moduleName, "libs-only-other")
	result.linkoptions = { result.linkoptions }
	result.versionnumber = version.tonumber(result)
	
	local I = this.getelement(moduleName, "cflags-only-I")
	result.includedirs = parseoptionlines(I, "-I")
	result.sysincludedirs = parseoptionlines(I, "-isystem ")
	
	local L = this.getelement(moduleName, "libs-only-L")
	result.libdirs = parseoptionlines(L, "-L")
	
	local l = this.getelement(moduleName, "libs-only-l")
	result.links = parseoptionlines(l, "-l")
	
	setmetatable(result, { __call = setupproject })
	return result
end

return this