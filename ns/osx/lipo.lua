--[[
ns-premake - lipo.lua
lipo utility wrapper

Copyright © 2013,2014 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local core = require("ns.core")
local cmd = require("ns.cmd")
local lipo = cmd.lipo

local this = {
	exists = lipo.exists
}

function this.merge(inputfiles, outputfile)
	if not type(inputfiles) == "table"
	then
		error ("Invalid inputfiles argument. Table expected")
	end

	if not #inputfiles > 1
	then
		error ("Invalid inputfiles argument. At least 2 elements expected")
	end

	local exitcode = lipo.execute(table.implode(inputfiles, '"', '"', ' ') .. ' -create -output "' .. outputfile .. '"')
	return (exitcode == 0)
end

function this.hasarch(path, arch)
	--[[


	-- Indicates if the file @param path contains the architecture @param arch


	-- @param path File to check


	-- @param arch Architecture to look into @param path


	--]]

	local exitcode = lipo.execute('"' .. path .. '" -verify_arch ' .. arch)
	return (exitcode == 0)
end

function this.archs(path)
	local out, exitcode = lipo.outputof('-info "' .. path .. '"')
	out = string.explode(out, '\n')
	local res = {}

	if not type(out) == "table" then return res end

	out = out[1]

	local archPattern = "(.*)%s([%a%d_-]+)%s*$"
	local found = true
	while found
	do
		found = false
		local remaining, p = string.gsub(out, archPattern, "%1")
		local arch, q = string.gsub(out, archPattern, "%2")
		out = remaining
		if p ~= 0
		then
			found = true
			table.insert(res, arch)
		end
	end

	return res
end

setmetatable (this, {
	__call = function (t, argstring) lipo.outputof(argstring) end
})

return this
