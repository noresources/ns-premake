--[[
ns-premake - otool.lua
otool utility wrapper

Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local core = require("ns.core")
local cmd = require("ns.cmd")
local otool = cmd.otool

local this = {
	exists = otool.exists
}

function this.links(path)
	-- Get linked objects
	-- @param path
	
	local out, exitcode = otool('-L "' .. path .. '"')
	out = string.explode(out, '\n')
	local res = {}
	local pattern = '%s*((.-)%s%(compatibility version ([0-9.]+), current version ([0-9.]+)%))'
	local frameworkpattern = '.-/(%w+)%.framework/Versions/(.-)/(%w+)$'
	local p2 = 'compatibility version ([0-9.]+)'
	for i = 3, #out
	do
		for line in out[i]:gmatch(pattern)
		do
			local key = line:gsub(pattern, '%2')
			local m = string.gsub(line, pattern, '%3')
			local c = string.gsub(line, pattern, '%4')
			local f = string.gmatch(key, frameworkpattern)
			local fwname, fwversion, fwobject = f()
			local props = {
				compatibilityversion = m,
				currentversion = c,
				kind = 'SharedLib',
				name = ''
			}
			
			if (type(fwname) == 'string') and (fwname == fwobject)
			then
				props.kind = 'Framework'
				props.name = fwname
				props.version = fwversion
			else
				local sharedlibpattern = '.*lib([^.]+)(%.(.*))%.dylib$'
				props.name = string.gsub(key, sharedlibpattern, '%1')
				props.version = string.gsub(key, sharedlibpattern, '%3')
			end
		
			res[key] = props
		end
	end
	
	return res
end

function this.id(path)
	local out = otool('-D "' .. path .. '"')
	out = string.explode(out, '\n')
	return out[2]
end

return this