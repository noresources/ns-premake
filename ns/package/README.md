Recipes to gather additional informations for packages found through the premake `os.findlib` function.

A recipe module should define a `result = postprocess(result, specs)` where
* `result` is the returned value of `package.get(specs)`
* 'specs` is the package specification given to `package.get(specs)`

This method should be used to modify and extend informations stored in `result`.