--[[
ns-premake - libcurl.lua
Find libcurl package information on system

Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local core = require ("ns.core")

local this = {
}

function this.postprocess(result, specs)
	--- Post process library specs found by ns.package.get(specs)
	if not (result.exists)
	then
		return result
	end
	
	local includedir = nil
	local curlverpath = nil
	for _, p in ipairs(result.includedirs)
	do
		curlverpath = p .. "/curl/curlver.h"
		if os.isfile(curlverpath)
		then
			includedir = p
			break
		else
			curlverpath = nil
		end
	end
	
	if (includedir == nil) 
		or (curlverpath == nil)
	then
		error ("Unable to find curlver.h in include dirs")
	end
	
	-- Remove useless include dirs
	result.includedirs = { includedir }
	
	local curlverfile = io.open(curlverpath)
	local curlvercontent = curlverfile:read('*all')
	local patterns = {
		major = "#define%s+LIBCURL_VERSION_MAJOR%s+([0-9]+)",
		minor = "#define%s+LIBCURL_VERSION_MINOR%s+([0-9]+)",
		patch = "#define%s+LIBCURL_VERSION_PATCH%s+([0-9]+)",
	}

	for key, pattern in pairs(patterns)
	do
		match = string.gmatch(curlvercontent, pattern)
		result[key] = match()
	end
		
	curlverfile:close()
		
	result.versionnumber = core.version.tonumber(result)
	result.versionstring = core.version.tostring(result)
	
	return result
end

----------------------------------------------------------------------------
return this
