--[[
ns-premake - pcre.lua
- Find PCRE library installation(s)
- Create a custom build project

Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local package = require ("ns.package")
local core = require ("ns.core")

local pkg = package.get("libpcre")

local this = {
	exists = pkg.exists
}

function this.getinstallation()
	return pkg
end

local syncedfiles = {
	"config.h",
	"pcre.h",
	"pcre_chartables.c"
}

function this.getsourceversion(sourcedir)
	-- Get pcre library version from source file
	-- @param sourcedir Path of the PCRE source code
	-- @return A hash table containing the following keys
	-- - major
	-- - minor
	-- - patch
	-- - versionstring
	-- - versionnumber
	--
	local result = {
		versionstring = "",
		versionnumber = 0,
		major = 0,
		minor = 0,
		patch = 0
	}

	local filepath = sourcedir .. "/pcre.h.generic"
	if os.isfile(filepath)
	then
		local file = io.open(filepath)
		local filecontent = file:read('*all')
		local patterns = {
			major = "#define%s+PCRE_MAJOR%s+([0-9]+)",
			minor = "#define%s+PCRE_MINOR%s+([0-9]+)",
		}

		for key, pattern in pairs(patterns)
		do
			match = string.gmatch(filecontent, pattern)
			result[key] = match()
		end

		file:close()
	else
		error("'" .. filepath .. "' not found in pcre source directory")
	end

	result.versionnumber = core.version.tonumber(result)
	result.versionstring = core.version.tostring(result)

	return result
end

function this.getprojectinfo(options)
	-- @param options Associative array of options
	-- 	- sourcedir: (mandatory) Path of PCRE library source code
	-- 	- location: Location of generated build scripts
	-- 	- targetdir: location of built files
	--	- system: (bool). If true, use sysincludedirs rather than includedirs

	if type(options) ~= "table"
	then
		if type(options) == "string"
		then
			options = { sourcedir = options }
		else
			error ("Invalid argument. Table expected")
		end
	end

	if (type(options.sourcedir) ~= "string") or not (os.isdir(options.sourcedir))
	then
		error ("Invalid source path '" ..  tostring(options.sourcedir) .. "'")
	end

	options.sourcedir = path.getabsolute(options.sourcedir)

	local info = {
		kind = premake.STATICLIB,
		language = "C",
		location = options.sourcedir .. "/build",
		targetdir = options.sourcedir .. "/build",
		defines = {"HAVE_CONFIG_H"}
	}
	
	local includedirkey = "includedirs"
	if (options.system)
	then
		includedirkey = "sysincludedirs"
	end
	
	info[includedirkey] = { options.sourcedir }

	-- User defined options
	if type(options) == "table"
	then
		for _, k in pairs ({
			"location",
			"targetdir",
			"language"
		})
		do
			if options[k] ~= nil
			then
				info[k] = options[k]
			end
		end
	end

	-- Normalize paths
	for _, k in ipairs ({
		"location",
		"targetdir"
	})
	do
		info[k] = path.getabsolute(info[k])
	end

	table.insert(info[includedirkey], info.location)

	local syncfilecommands = '@' .. core.syncfilecommands ({
		{  source = options.sourcedir .. '/config.h.generic',
			destination = info.location .. '/config.h' },
		{ source = options.sourcedir .. '/pcre.h.generic',
			destination = info.location .. '/pcre.h' },
		{ source = options.sourcedir .. '/pcre_chartables.c.dist',
			destination = info.location .. '/pcre_chartables.c' }
	})

	info.prebuildcommands = { syncfilecommands }

	-- @todo allow to select 8/16/32bit character-length version
	info.files = {}
	for _, f in ipairs ({
		"pcre_byte_order.c",
		"pcre_compile.c",
		"pcre_config.c",
		"pcre_dfa_exec.c",
		"pcre_exec.c",
		"pcre_fullinfo.c",
		"pcre_get.c",
		"pcre_globals.c",
		"pcre_jit_compile.c",
		"pcre_maketables.c",
		"pcre_newline.c",
		"pcre_ord2utf8.c",
		"pcre_refcount.c",
		"pcre_string_utils.c",
		"pcre_study.c",
		"pcre_tables.c",
		"pcre_ucd.c",
		"pcre_valid_utf8.c",
		"pcre_version.c",
		"pcre_xclass.c"
	})
	do
		table.insert(info.files, options.sourcedir .. "/" .. f)
	end

	table.insert(info.files, info.location .. "/pcre_chartables.c")

	info.buildoptions = {
		"-Wno-unused-function",
		"-Wno-unused-parameter",
		"-Wno-multichar"
	}

	return info
end

local function projectpreprocess(project, info, options)
	-- @param project Premake project
	-- @param info Result of getprojectinfo

	local pcre_stringpiece_filepath = options.sourcedir .. "/pcre_stringpiece.h.in"
	if not (os.isfile(pcre_stringpiece_filepath))
	then
		error 'PCRE file "pcre_stringpiece.h.in" not found'
	end

	local pcre_stringpiece_file = io.open(pcre_stringpiece_filepath, "r")
	local pcre_stringpiece_content = pcre_stringpiece_file:read('*all')
	pcre_stringpiece_file:close()

	-- Assumes the platform does not have all that things
	for k, v in pairs {
		["@pcre_have_type_traits@"] = 0,
		["@pcre_have_bits_type_traits@"] = 0
	}
	do
		pcre_stringpiece_content = string.gsub(pcre_stringpiece_content, k, v)
	end

	os.mkdir(info.location)

	pcre_stringpiece_filepath = info.location .. "/pcre_stringpiece.h"
	pcre_stringpiece_file = io.open(pcre_stringpiece_filepath, "w")
	pcre_stringpiece_file:write(pcre_stringpiece_content)
	pcre_stringpiece_file:close()

	return true
end

function this.project(name, options)
	local info = this.getprojectinfo(options)
	local pj = project(name)

	projectpreprocess(pj, info, options)

	for k, v in pairs (info)
	do
		if type(_G[k]) == "function"
		then
			_G[k](v)
		end
	end

	return pj, info
end

return this
