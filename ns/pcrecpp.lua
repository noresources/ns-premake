--[[
ns-premake - pcrecpp.lua
- Find PCRE C++ binding library (pcrecpp) installation(s)
- Create a custom build project

Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local package = require ("ns.package")
local pcre = require ("ns.pcre")

local pkg = package.get("libpcrecpp")

local this = {
	exists = pkg.exists,
	getsourceversion = pcre.getsourceversion
}

function this.getinstallation()
	return pkg
end

function this.getprojectinfo(options)
	-- @param sourcePath Location of PCRE source distribution
	-- @param options Associative array of options
	-- 	- sourcedir: (mandatory) Path of PCRE library source code
	-- 	- location: Location of generated build scripts
	-- 	- targetdir: location of built files
	-- 	- pcretargetdir: Location of pcre targetdir (default: targetdir)
	--	- system: (bool). If true, use sysincludedirs rather than includedirs

	if type(options) ~= "table"
	then
		if type(options) == "string"
		then
			options = { sourcedir = options }
		else
			error ("Invalid argument. Table expected")
		end
	end

	if (type(options.sourcedir) ~= "string")
		or not (os.isdir(options.sourcedir))
	then
		error ("Invalid source path '" ..  tostring(options.sourcedir) .. "'")
	end

	options.sourcedir = path.getabsolute(options.sourcedir)

	local info = {
		kind = premake.STATICLIB,
		language = "C++",
		location = options.sourcedir .. "/build",
		targetdir = options.sourcedir .. "/build",
		defines = {"HAVE_CONFIG_H"},
		links = {"pcre"},
		files = {}
	}

	local includedirkey = "includedirs"
	if (options.system)
	then
		includedirkey = "sysincludedirs"
	end

	info[includedirkey] = { options.sourcedir }

	-- User defined options
	if type(options) == "table"
	then
		for _, k in pairs ({
			"location",
			"targetdir"
		})
		do
			if options[k] ~= nil
			then
				info[k] = options[k]
			end
		end
	end

	-- Normalize paths
	for _, k in ipairs ({
		"location",
		"targetdir"
	})
	do
		info[k] = path.getabsolute(info[k])
	end

	table.insert(info[includedirkey], info.location)
	if (type(options.pcretargetdir) == "string")
		and (os.isdir(options.pcretargetdir))
	then
		local p = path.getabsolute(options.pcretargetdir)
		if p ~= info.targetdir
		then
			table.insert(info[includedirkey], p)
		end
	end

	table.insert(info.files, options.sourcedir .. "/pcrecpp.cc")

	info.buildoptions = {
		"-Wno-missing-field-initializers",
		"-Wno-unused-parameter"
	}

	return info
end

function this.projectpreprocess(project, info, options)
	-- @param project Premake project
	-- @param info Result of getprojectinfo

	local pcrecppargfilepath = options.sourcedir .. "/pcrecpparg.h.in"
	if not (os.isfile(pcrecppargfilepath))
	then
		error 'PCRE file "pcrecpparg.h.in" not found'
	end

	local pcrecppargfile = io.open(pcrecppargfilepath, "r")
	local pcrecppargcontent = pcrecppargfile:read('*all')
	pcrecppargfile:close()

	-- Assumes the platform have (u)long long
	for _, p in ipairs {
		"@pcre_have_long_long@",
		"@pcre_have_ulong_long@"
	}
	do
		pcrecppargcontent = string.gsub(pcrecppargcontent, p, "1")
	end

	os.mkdir(info.location)

	pcrecppargfilepath = info.location .. "/pcrecpparg.h"
	pcrecppargfile = io.open(pcrecppargfilepath, "w")
	pcrecppargfile:write(pcrecppargcontent)
	pcrecppargfile:close()

	return true
end

function this.project(name, options)
	local info = this.getprojectinfo(options)
	local pj = project(name)

	this.projectpreprocess(pj, info, options)

	for k, v in pairs (info)
	do
		if type(_G[k]) == "function"
		then
			_G[k](v)
		end
	end

	return pj, info
end

return this


