--[[
ns-premake - ld.lua
ld binary utility
Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local core = require("ns.core")
local cmd = require("ns.cmd")
local ld = cmd.ld

local this = {
	exists = ld.exists
}

this.searchpaths = function()
	-- List library search paths
	-- # Call ld --verbose
	-- # look for SEARCH_DIR("path");

	if not this.exists
	then
		return {}
	end

	local out = ld.outputof("--verbose")
	local paths = {}
	if type(out) ~= "string"
	then
		return paths, "Failed to call 'ld --verbose'"
	end

	for match in out:gmatch("SEARCH_DIR%(\"=(.-)\"%);")
	do
		table.insert(paths, match)
	end

	return paths
end

return this
