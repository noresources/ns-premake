--[[
ns-premake - ld.lua
ld binary utility
Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local core = require("ns.core")
local cmd = require("ns.cmd")
local nm = cmd.nm

local this = {
	exists = nm.exists
}

this.getsymbols = function(filepath, targetType, fullinfo)
	-- @param filepath
	-- @param targetType One of SharedLib, StaticLib
	local symbols = {}
	if not this.exists
	then
		return out
	end

	if type(fullinfo) ~= "boolean"
	then
		fullinfo = false
	end

	local arguments = ""
	if targetType == "SharedLib"
	then
		arguments = arguments .. " -D"
	end

	arguments = arguments .. " \"" .. filepath .. "\""

	local out, exitcode = nm.outputof(arguments)
	if exitcode == 0
	then
		out = string.explode(out, '\n')
		for _, line in ipairs(out)
		do
			line = string.gsub(line, "%s*(.*)", "%1")
			local parts = string.explode(line, " ")
			if fullinfo
			then
				table.insert(symbols, {type = parts[#parts - 1], symbol = parts[#parts]})
			else
				table.insert(symbols, parts[#parts])
			end
		end
	end

	return symbols
end

this.hassymbol = function(filepath, targetType, symbol)
	local symbols = this.getsymbols(filepath, targetType, false)
	return table.exists(symbols, symbol)
end

return this
