--[[
ns-premake - core.lua
Base functions

Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local this = {
}

function this.outputfirstlineof(cmd)
	-- Get only the first line of os.outputof
	
	local res, exitcode = os.outputof(cmd)
	if (type(res) == "string")
	then
		res = string.explode(res, "\n")
		res = res[1]
	end
	
	return res, exitcode
end

function this.syncfilecommands(copyrules)
--- Return a system-dependant command line to copy files given by @param copyrules
--  @param copyrules A hash table { source = "string", destination = "string" } or 
--  a table of these hash table 
--  
	local syncCommand = "cp"
		
	local res = {}
	if os.is("windows")
	then
		syncCommand = "cp"
	elseif os.is(premake.MACOSX)
	then
		-- rsync is installed on most OSX versions 
		syncCommand = "rsync -lprt"
	elseif os.is("bsd") or os.is(premake.LINUX)
	then
		table.insert(res, 
			'which rsync 1>/dev/null 2>&1 && syncCommand="rsync -lprt" || syncCommand="cp -r"'
		)
		
		syncCommand = "$${syncCommand}"
	end
	
	local rules = copyrules
	
	if type(copyrules.source) == "string"
		and type(copyrules.destination) == "string"
	then
		rules = { copyrules }
	end
	
	for _, r in ipairs(rules)
	do
		table.insert(res, syncCommand .. ' "' .. r.source .. '" "' .. r.destination .. '"')
	end
	
	return table.implode(res, "", ";", "\\\n\t")
end

function this.targetsuffix(targetType, osName)
	-- Extension suffix of a a given target type
	if type(osName) ~= "string"
	then
		osName = os.get()
	end
	
	if targetType == "ConsoleApp"
	then
		if osName == "windows"
		then
			return ".exe"
		end
	elseif targetType == "SharedLib"
	then
		if osName == "windows"
		then
			return ".dll"
		elseif osName == premake.LINUX
		then
			return ".so"
		elseif osName == premake.MACOSX
		then
			return ".dylib"
		end
	elseif targetType == premake.STATICLIB
	then
		if osName == premake.WINDOWS
		then
			return ".lib"
		elseif (osName == premake.LINUX) or (osName == premake.MACOSX)
		then
			return ".a"
		end
	elseif targetType == "WindowedApp"
	then
		if osName == "windows"
		then
			return ".exe"
		elseif osName == premake.MACOSX
		then
			return ".app"
		end
	end
	
	return ""
end

function this.moduleexists(modulepath)
--- Check if a given lua module can be found
	if package.loaded[modulepath]
	then
		return true
	else
		for _, searcher in ipairs(package.searchers or package.loaders) 
		do
			local loader = searcher(modulepath)
			if type(loader) == 'function' 
			then
				return true
			end
		end
	end
	
	return false
end

---------------------------------------------------------

local requireroot = ...

local submodules = {
}

local function onindex(t, modulename)
	if type(modulename) ~= "string"
	then
		error ("Invalid call (" .. tostring(modulename) .. " is not a string)")
	end
	
	local m = submodules[modulename]
	if not (m)
	then
		m = {
			exists = false,
			module = nil
		}
		
		submodules[modulename] = m
		
		local modulepath = requireroot .. "." .. modulename
		if package.loaded[modulepath]
		then
			m.exists = true
			m.module = package.loaded[modulepath]
		else
			if this.moduleexists(modulepath)
			then
				m.exists = true
				m.module = require(modulepath)
			end
		end
	end

	if m.exists
	then
		return m.module
	else
		error ("Submodule '" .. modulename .. "' not found")
	end
end

local function onnewindex(t, module, v)
	error ("Read only property " .. tostring(module))
end

setmetatable (this, {
	__index = onindex,
	__newindex = onnewindex
})

----------------------------------------------------------------------------
return this
