--[[
ns-premake - package.lua
Find installed packages

Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local libmoduleroot = ...
local core = require("ns.core")
local pkgconfig = require("ns.pkgconfig")

local ld = {exists = false}
local nm = {exists = false}
if os.is("linux")
then
	ld = require("ns.linux.ld")
	nm = require("ns.linux.nm")
end

local appendunique = function(t, v)
	for _, value in ipairs (t)
	do
		if value == v
		then
			return
		end
	end
	
	table.insert(t, v)
end

local prependunique = function(t, v)
	for _, value in ipairs (t)
	do
		if value == v
		then
			return
		end
	end
	
	table.insert(t, 1, v)
end

local this = {}

local function setupproject(pkg)
	if not (pkg.exists)
	then
		return
	end

	for _, key in ipairs({
			'sysincludedirs', 
			'includedirs', 
			'buildoptions', 
			'syslibdirs', 
			'libdirs', 
			'linkoptions', 
			'links', 
			'runpathdirs'
		})
	do
		local v = pkg[key]
		if (type(v) == 'string' or type(v) == 'table')
			and (#v > 0)
		then
			_G[key](v)
		end
	end
end

local finderMethods = {}

finderMethods["pkg-config"] = function(specs)
	return pkgconfig.get(specs.modulename)
end

finderMethods["findlib"] = function(specs)
	local result = {
		exists = false,
		sysincludedirs = {},
		includedirs = {},
		buildoptions = {},
		syslibdirs = {},
		libdirs = {},
		linkoptions = {},
		runpathdirs = {},
		links = {},
		versionnumber = 0,
		versionstring = "",
		minor = 0,
		major = 0,
		patch = 0
	}

	local includedirkey = 'includedirs'
	local libdirkey = 'libdirs'
	if (specs.system)
	then
		includedirkey = 'sysincludedirs'
		libdirkey = 'syslibdirs'
	end

	-- Use premake builtin
	local libdir = os.findlib(specs.name, specs.libdirs)
	if libdir ~= nil
		and #libdir
	then
		result.exists = true
		
		prependunique(result[libdirkey], libdir)
		
		local includedir = libdir:gsub("(.*)/lib.*", "%1") .. "/include"
		prependunique(result[includedirkey], includedir)
		
		-- Also add (sys)includedirs from finder specs
		for _, key in ipairs ({
				'includedirs', 
				'sysincludedirs'
			})
		do
			for _, p in ipairs(specs[key])
			do
				if os.isdir (p)
				then
					appendunique(result[key], p)
				end
			end
		end
		
		appendunique(result.links, specs.name)
	end

	-- Special tasks for known modules
	libmodulepath = libmoduleroot .. "." .. specs.modulename
	if core.moduleexists(libmodulepath)
	then
		local libmodule = require(libmodulepath)
		result = libmodule.postprocess(result, specs)
	end

	return result
end

----------------------------------------------------------------------------
-- Public API

function this.get(packagespecs)
	-- @param packagespecs A package name or a table with the following keys
	-- - name = library short name
	-- - modulename = pkg-config modulename (default: name)
	-- - header = name of a header file to look for in the library include directory
	-- - includedirs = additional include directories
	-- - libdirs = additional library search paths
	-- - kinds = library kind ("SharedLib", "StaticLib" or both)
	-- - symbols = a list of symbol names to look into library (if found)
	-- - methods = the list of methods to use to find the package ("pkg-config" and/or "findlib")
	-- - system = consider the package as a system package. If @c true (the default),
	-- 		includedirs & libdirs instructions will be replaced by sysincludedirs & syslibdirs

	local defaultMethods = { "pkg-config" , "findlib" }
	local defaultKinds = {premake.SHAREDLIB, premake.STATICLIB }

	local finderspecs = {
		-- A bunch of default system paths
		sysincludedirs = {
			"/opt/local/include",
			"/opt/include",
			"/usr/local/include",
			"/usr/include",
			"/usr/X11/include"
		},
		includedirs = {},
		symbols = {},
		kinds = defaultKinds,
		libdirs = {},
		methods = defaultMethods,
		system = true
	}
	
	if type(packagespecs) == 'string'
	then
		finderspecs.modulename = packagespecs
	elseif type(packagespecs) == "table"
	then
		for _, s in ipairs({
				'header', 
				'modulename', 
				'name', 
				'system'
			})
		do
			if (packagespecs[s] ~= nil)
			then
				finderspecs[s] = packagespecs[s]
			end
		end

		for _, key in ipairs({'symbols'})
		do
			if type(packagespecs[key]) == 'string'
			then
				finderspecs[key] = { packagespecs[key] }
			elseif type(packagespecs[key]) == 'table'
			then
				for _, p in ipairs(packagespecs[key])
				do
					table.insert(finderspecs[key], p)
				end
			else
				finderspecs[key] = {}
			end
		end

		if type(packagespecs.includedirs) == "string"
		then
			table.insert(finderspecs.includedirs, 1, packagespecs.includedirs)
		elseif type(packagespecs.includedirs) == "table"
		then
			local pi = 1
			for _, p in ipairs(packagespecs.includedirs)
			do
				table.insert(finderspecs.includedirs, pi, p)
				pi = pi + 1
			end
		end

		if type(packagespecs.methods) == "string"
		then
			finderspecs.methods = { packagespecs.methods }
		elseif type(packagespecs.methods) == "table"
		then
			finderspecs.methods = packagespecs.methods
		end

		if type(packagespecs.kinds) == "string"
		then
			finderspecs.kinds = { packagespecs.kinds }
		elseif type(packagespecs.kinds) == "table"
		then
			finderspecs.kinds = packagespecs.kinds
		end

		if type(packagespecs.libdirs) == "string"
		then
			finderspecs.libdirs = { packagespecs.libdirs }
		elseif type(packagespecs.libdirs) == "table"
		then
			finderspecs.libdirs = packagespecs.libdirs
		end
	else
		error ("Invalid argument: string or table expected")
	end

	if not finderspecs.modulename
	then
		finderspecs.modulename = finderspecs.name
	end

	if not finderspecs.name
	then
		if finderspecs.modulename
		then
			finderspecs.name = finderspecs.modulename:gsub("^lib(.+)", "%1")
		else
			error ("Invalid package specification: name is mandatory")
		end
	end

	local result = {
		exists = false
	}
	
	local includedirkey = 'includedirs'
	local libdirkey = 'libdirs';
	if (packagespecs.system)
	then
		includedirkey = 'sysincludedirs'
		libdirkey = 'syslibdirs'
	end

	for _, method in ipairs(finderspecs.methods)
	do
		if type(finderMethods[method]) ~= "function"
		then
			error ("Invalid package finder method '" .. method .. "'")
		end

		result = finderMethods[method](finderspecs)
		result.method = method

		if result.exists
			and finderspecs.header
		then
			local found = false
			-- Search header in result include dirs
			for _, key in ipairs ({"includedirs", "sysincludedirs"})
			do
				found = false
				if (type(result[key]) == "table")
				then
					for _, p in ipairs(result[key])
					do
						if os.isfile(p .. "/" .. finderspecs.header)
						then
							found = true
							break
						end
					end
				end

				if (found)
				then
					break
				end
			end

			if not found
			then
				-- Search header in specs
				for _, key in ipairs ({"includedirs", "sysincludedirs"})
				do
					for _, p in ipairs(finderspecs[key])
					do
						if os.isfile(p .. "/" .. finderspecs.header)
						then
							prependunique(result[includedirkey], p)
							found = true
							break
						end
					end
				end
			end

			if not found
			then
				result.exists = false
			end
		end

		-- Symbol search
		if result.exists
			and (#finderspecs.symbols > 0)
			and ld.exists
			and nm.exists
		then
			local ldpaths = ld.searchpaths()
			local libdirs = ldpaths
			if type(result[libdirkey]) == "table"
			then
				libdirs = result[libdirkey]
			end

			local symbolsFound = false
			for _, type in ipairs(finderspecs.kinds)
			do
				local pattern = finderspecs.name .. core.targetsuffix(type)
				if not string.startswith(pattern, "lib")
				then
					pattern = "lib" .. pattern
				end

				for _, path in ipairs(libdirs)
				do
					local matches = os.matchfiles(path .. "/" .. pattern)

					for _, filepath in ipairs(matches)
					do
						symbolsFound = false
						for _, symbol in ipairs(finderspecs.symbols)
						do
							symbolsFound = (symbolsFound and nm.hassymbol(filepath, type, symbol))
						end
					end

					if symbolsFound
					then
						break
					end
				end

				if symbolsFound
				then
					break
				end
			end

			result.exists = symbolsFound
		end

		if result.exists
		then
			break
		end
	end

	setmetatable(result, { __call = setupproject })
	return result
end

function this.exists(packagedesc)
	local result = this.get(packagedesc)
	return result.exists
end

----------------------------------------------------------------------------
return this
