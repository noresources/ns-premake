--[[
ns-premake - sqlite3.lua
Find sqlite3 installation(s)

Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local package = require ("ns.package")

local pkg = package.get("sqlite3")
local core = require ("ns.core")

local this = {
	exists = pkg.exists
}

function this.getsourceversion(sourcedir)
-- Get pcre library version from source file
-- @param sourcedir Path of the sqlite source code
-- @return A hash table containing the following keys
-- - major
-- - minor
-- - patch
-- - versionstring
-- - versionnumber

	local result = {
		versionstring = "",
		versionnumber = 0,
		major = 0,
		minor = 0,
		patch = 0
	}
	
	local filepath = sourcedir .. "/sqlite3.h"
	if os.isfile(filepath)
	then
		local file = io.open(filepath)
		local filecontent = file:read('*all')
		local patterns = {
			versionstring = "#define%s+SQLITE_VERSION%s+\"([0-9.]+)\"",
			versionnumber = "#define%s+SQLITE_VERSION_NUMBER%s+([0-9]+)"
		}

		for key, pattern in pairs(patterns)
		do
			match = string.gmatch(filecontent, pattern)
			result[key] = match()
		end

		file:close()
	else
		error("'" .. filepath .. "' not found in pcre source directory")
	end
	
	for part, value in pairs(core.version.split(result.versionnumber))
	do
		result[part] = value
	end
	
	return result
end

function this.getinstallation()
	-- Legacy
	return pkg
end

return this