--[[
ns-premake - postgresql.lua
Find PostgreSQL installation(s)

Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local core = require("ns.core")
local version = core.version

local this = {
}

local pkgsearchpath = {
	-- UNIX/OSX path
	"/usr/local/pgsql",
	"/opt/local/pgsql",
	"/usr/local",
	"/opt/local",
	"/opt",
	"/usr",
}

-- <prefix>/lib/postgresql<version>
for i, p in pairs({ "/usr/lib", "/usr/local/lib", "/opt/lib",	 "/opt/local/lib" })
do
	local matches = os.matchdirs(p .. "/postgresql*")
	for k, v in pairs(matches)
	do
		table.insert(pkgsearchpath, 1, v)
	end
end

if os.is("macosx")
then
	-- PostgreSQL installed package
	local matches = os.matchdirs("/Library/PostgreSQL/*")
	for k, v in pairs(matches)
	do
		table.insert(pkgsearchpath, 1, v)
	end
	
end

local installations = {}

for k, v in pairs(pkgsearchpath)
do
	local cfgbin = v .. "/bin/pg_config"
	if os.isfile(cfgbin)
	then
		local pgversion = core.outputfirstlineof(cfgbin .." --version")
		local pattern = ".*[ \t]+(([%d]+)\.([%d]+)\.([%d]+))$"
		local i = {
	  		versionstring = string.gsub(pgversion, pattern, "%1"),
	  		libdir = core.outputfirstlineof(cfgbin .." --libdir"),
	  		includedir = core.outputfirstlineof(cfgbin .." --includedir"),
	  		linkoptions = ""
	  }
	  
	  local vparts = version.toparts(i.versionstring)
	  for k, v in pairs(vparts)
	  do
	  	i[k] = v
	  end
	  
	  i.versionnumber = version.tonumber(i)
	  
	  -- @todo check the exact version of pg_config where the --libs was added
	  if i.major > 7
	  then
	  	i.linkoptions = core.outputfirstlineof(v .. "/bin/pg_config --libs")
	  end
	  
	  table.insert(installations, i)
	end
end

-- Public API

this.exists = false
if (#installations > 0)
then
	this.exists = true
end

function this.getinstallation(filters, strict)
	-- Get the first installation info matching filters
	if not(this.exists)
	then
		return nil
	end
	
	if type(strict) ~= "boolean"
	then
		strict = false
	end
	
	if not(type(filters) == "table")
	then
		return installations[1]	
	end
	
	local keys = {
		versionstring = "string",
		versionnumber = "number",
		major = "number",
		minor = "number",
		patch = "number" 
	}

	local match = true
	local c = 0
	local bestc = 0 
	local best = installations[1]	
	for k, i in pairs(installations)
	do
		match = true
		c = 0
		for f, t in pairs(keys)
		do
			if (type(filters[f]) == t)
			then
				if (filters[f] == i[f]) 
					or ((t == "string") and ((string.match(i[f], filters[f])) ~= nil))
				then
					c = c + 1
				end
			end
		end
		
		local filterCount = 0
		for k, v in pairs(filters)
		do
			filterCount = filterCount + 1
		end

		if (c == filterCount)
		then
			return i
		elseif (c > bestc)
		then
			bestc = c
			best = i
		end
	end
		
	if strict
	then
		return nil
	end
	
	return best
end

return this
