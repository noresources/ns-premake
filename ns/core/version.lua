--[[
ns-premake - version.lua
Version number & string utilities

Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local this = {}

function this.split(versionspec)
--- Get the version number broken out in major, minor and patch level number

	local result = {
		minor = 0,
		major = 0,
		patch = 0
	}

	if type (versionspec) == "number"
	then
		result.major = math.floor(versionspec / 10000);
		result.minor = math.floor(versionspec / 100) % 100;
		result.patch = versionspec % 100;

		return result
	end

	if type(versionspec) == "table"
	then
		local result = {
			minor = 0,
			major = 0,
			patch = 0
		}

		for k, v in pairs(result)
		do
			local p = versionspec[k]
			result[k] = tonumber(p)
		end

		return result
	end

	if type (versionspec) ~= "string"
	then
		error ("Invalid argument: string or number expected, got " .. type(versionspec))
	end

	local parts = { "major", "minor", "patch" }
	local index = 1
	for number in string.gmatch(versionspec, "%d+")
	do
		if index > #parts
		then
			break
		end

		result[parts[index]] = tonumber(number)
		index = index + 1
	end

	return result
end

this.toparts = this.split

function this.tonumber(versionspec)
--- Create a version number from version string or broken-out version parts
--- @return A number following the formula (Major * 10000 + Minor * 100 + Patch)

	if type (versionspec) == "number"
	then
		return versionspec
	end

	local parts = {major = 0, minor = 0, patch = 0}

	if (type(versionspec) == "string")
	then
		parts = this.split(versionspec)
	elseif type(parts) == "table"
	then
		for k, v in pairs(parts)
		do
			if versionspec[k]
			then
				parts[k] = versionspec[k]
			end
		end
	else
		error ("Invalid argument: table or string expected, got " .. type(versionspec))
	end

	if type(parts) ~= "table"
	then

	end

	return (parts.patch + (parts.minor * 100) + (parts.major * 10000))
end

function this.tostring(versionspec, fullstring)
--- Convert a version number to a dot-separated string 
	local parts = versionspec

	if (type(versionspec) == "number")
		or (type(versionspec) == "string")
	then
		parts = this.split(parts)
	end

	if type(parts) ~= "table"
	then
		error ("Invalid argument: table or number expected, got " .. type(versionspec))
	end

	local str = ""
	local M = parts.major
	local m = parts.minor
	local p = parts.patch

	local Ms = tostring(M)
	local ms = tostring(m)
	local ps = tostring(p)

	if M and #Ms
	then
		str = Ms
	else
		str = "0"
	end

	if m and #ms and (ms ~= "0")
	then
		str = str .. "." .. ms
	elseif fullstring or (p and #ps and (ps ~= "0"))
	then
		str = str .. ".0"
	end

	if p and #ps and (ps ~= "0")
	then
		str = str .. "." .. ps
	elseif fullstring
	then
		str = str .. ".0"
	end

	return str
end

----------------------------------------------------------------

return this