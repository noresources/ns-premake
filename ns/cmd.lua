--[[
ns-premake - cmd.lua
Generic console commands wrapper

Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE
--]]

local core = require("ns.core")

local commands = {}
local havewhich = false

if not os.is("windows")
then
	havewhich = true
end

local this = 
{
}

local function executecommand(path, func, args)
	if type(args) == "string" and (#args > 0)
	then
		return func(path .. " " .. args)
	end
	
	return func(path)
end

local function makecommand(name)
	
	local cmd = {
		exists = false
	}
	
	if havewhich
	then
		if tonumber(os.execute("which " .. name .. " 1>/dev/null 2>&1")) == 0
		then
			cmd.exists = true
			cmd.path = core.outputfirstlineof("which " .. name)
		end
	else
		local paths = os.getenv("PATH")
		if type(paths) == "string"
		then
			for path in string.gmatch(paths, "(.-):")
			do
				local p = path .. "/" .. name
				if os.isfile(p)
				then
					cmd.exists = true
					cmd.path = p
					break
				end 
			end
		end
	end
	
	if cmd.exists
	then
		cmd.execute = function (argstring) return executecommand(cmd.path, os.execute, argstring) end
		cmd.outputof = function (argstring) return executecommand(cmd.path, os.outputof, argstring) end
		cmd.outputfirstlineof = function (argstring) return executecommand(cmd.path, core.outputfirstlineof, argstring) end
	else
		cmd.execute = function () error("execute: " .. name .. " not found") end
		cmd.outputof = function () error("outputof: " .. name .. " not found") end
		cmd.outputfirstlineof = function () error("outputfirstlineof: " .. name .. " not found") end
	end
	
	setmetatable (cmd, { __call = function (c, argstring) return cmd.outputof(argstring) end })
		
	return cmd
end

local function onindex(t, key)
	local cmd = rawget(commands, key)
	if cmd == nil
	then
		cmd = makecommand(key)
		rawset(commands, key, cmd)
	end
	
	return cmd
end

local function onnewindex(t, key, value)
	error (key .. " is a read only property")
end

setmetatable (this, {
	__index = onindex, 
	__newindex = onnewindex 
})

return this