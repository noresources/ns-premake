ns-premake
===========
Lua utility modules for the premake5 tool

Copyright © 2013-2016 by Renaud Guillard (dev@nore.fr)
Distributed under the terms of the MIT License, see LICENSE

## Prerequisites
ns-premake works alongside [premake5](https://bitbucket.org/premake/premake-dev)

## Usage
First, you need to add the ns-premake sources accessible from the [Lua](http://www.lua.org/) package loader.
Add the following line in your main premake script (generally called premake5.lua)

     package.path = path_to_premake_modules_root .. "/?.lua;" .. package.path
     
Then, you can load modules the standard way

    local pgsql = require("ns.postgresql")
    if pgsql.exists
    then
        local defaultInstallation = pgsql.getinstallation()
        print("pgsql version: " .. defaultInstallation.versionstring)
        print("pgsql include directory: " .. defaultInstallation.includedir)
        
        -- Add PostgresSQL-dependent stuff...
    end
